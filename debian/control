Source: haskell-free
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders: Iulian Udrea <iulian@linux.com>
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8.4.3),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-comonad-dev (>= 4),
 libghc-comonad-dev (<< 6),
 libghc-comonad-prof,
 libghc-distributive-dev (>= 0.2.1),
 libghc-distributive-prof,
 libghc-exceptions-dev (>= 0.6),
 libghc-exceptions-dev (<< 0.11),
 libghc-exceptions-prof,
 libghc-profunctors-dev (>= 4),
 libghc-profunctors-dev (<< 6),
 libghc-profunctors-dev (>= 4),
 libghc-profunctors-prof,
 libghc-semigroupoids-dev (<< 6),
 libghc-semigroupoids-dev (>= 4),
 libghc-semigroupoids-prof,
 libghc-transformers-base-dev (>= 0.4),
 libghc-transformers-base-dev (<< 0.5),
 libghc-transformers-base-prof,
Build-Depends-Indep: ghc-doc,
 libghc-comonad-doc,
 libghc-distributive-doc,
 libghc-exceptions-doc,
 libghc-profunctors-doc,
 libghc-semigroupoids-doc,
 libghc-transformers-base-doc,
Standards-Version: 4.4.0
Homepage: https://github.com/ekmett/free/
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-free
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-free]

Package: libghc-free-dev
Architecture: any
Depends: ${haskell:Depends}, ${misc:Depends}, ${shlibs:Depends}
Recommends: ${haskell:Recommends}
Suggests: ${haskell:Suggests}
Provides: ${haskell:Provides}
Description: Monads for free${haskell:ShortBlurb}
 Free monads are useful for many tree-like structures and domain specific
 languages.
 .
 A Monad n is a free Monad for f if every Monad homomorphism from n to another
 monad m is equivalent to a natural transformation from f to m.
 .
 Cofree comonads provide convenient ways to talk about branching streams and
 rose-trees, and can be used to annotate syntax trees.
 .
 A Comonad v is a cofree Comonad for f if every Comonad homomorphism another
 comonad w to v is equivalent to a natural transformation from w to f.
 .
 ${haskell:Blurb}

Package: libghc-free-prof
Architecture: any
Depends: ${haskell:Depends}, ${misc:Depends}
Recommends: ${haskell:Recommends}
Suggests: ${haskell:Suggests}
Provides: ${haskell:Provides}
Description: Monads for free${haskell:ShortBlurb}
 Free monads are useful for many tree-like structures and domain specific
 languages.
 .
 A Monad n is a free Monad for f if every Monad homomorphism from n to another
 monad m is equivalent to a natural transformation from f to m.
 .
 Cofree comonads provide convenient ways to talk about branching streams and
 rose-trees, and can be used to annotate syntax trees.
 .
 A Comonad v is a cofree Comonad for f if every Comonad homomorphism another
 comonad w to v is equivalent to a natural transformation from w to f.
 .
 ${haskell:Blurb}

Package: libghc-free-doc
Architecture: all
Section: doc
Depends: ${haskell:Depends}, ${misc:Depends}
Recommends: ${haskell:Recommends}
Suggests: ${haskell:Suggests}
Description: Monads for free${haskell:ShortBlurb}
 Free monads are useful for many tree-like structures and domain specific
 languages.
 .
 A Monad n is a free Monad for f if every Monad homomorphism from n to another
 monad m is equivalent to a natural transformation from f to m.
 .
 Cofree comonads provide convenient ways to talk about branching streams and
 rose-trees, and can be used to annotate syntax trees.
 .
 A Comonad v is a cofree Comonad for f if every Comonad homomorphism another
 comonad w to v is equivalent to a natural transformation from w to f.
 .
 ${haskell:Blurb}
